$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Feature1/001Login1.feature");
formatter.feature({
  "id": "login-action",
  "tags": [
    {
      "name": "@tag",
      "line": 20
    }
  ],
  "description": "",
  "name": "Login Action",
  "keyword": "Feature",
  "line": 21,
  "comments": [
    {
      "value": "#Author: your.email@your.domain.com",
      "line": 1
    },
    {
      "value": "#Keywords Summary :",
      "line": 2
    },
    {
      "value": "#Feature: List of scenarios.",
      "line": 3
    },
    {
      "value": "#Scenario: Business rule through list of steps with arguments.",
      "line": 4
    },
    {
      "value": "#Given: Some precondition step",
      "line": 5
    },
    {
      "value": "#When: Some key actions",
      "line": 6
    },
    {
      "value": "#Then: To observe outcomes or validation",
      "line": 7
    },
    {
      "value": "#And,But: To enumerate more Given,When,Then steps",
      "line": 8
    },
    {
      "value": "#Scenario Outline: List of steps for data-driven as an Examples and \u003cplaceholder\u003e",
      "line": 9
    },
    {
      "value": "#Examples: Container for s table",
      "line": 10
    },
    {
      "value": "#Background: List of steps run before each of the scenarios",
      "line": 11
    },
    {
      "value": "#\"\"\" (Doc Strings)",
      "line": 12
    },
    {
      "value": "#| (Data Tables)",
      "line": 13
    },
    {
      "value": "#@ (Tags/Labels):To group Scenarios",
      "line": 14
    },
    {
      "value": "#\u003c\u003e (placeholder)",
      "line": 15
    },
    {
      "value": "#\"\"",
      "line": 16
    },
    {
      "value": "## (Comments)",
      "line": 17
    },
    {
      "value": "#Sample Feature Definition Template",
      "line": 19
    }
  ]
});
formatter.scenario({
  "id": "login-action;successful-login-with-valid-credentials",
  "tags": [
    {
      "name": "@tag1",
      "line": 23
    }
  ],
  "description": "",
  "name": "Successful Login with Valid Credentials",
  "keyword": "Scenario",
  "line": 24,
  "type": "scenario"
});
formatter.step({
  "name": "User is on Home Page",
  "keyword": "Given ",
  "line": 25
});
formatter.step({
  "name": "User Navigate to LogIn Page",
  "keyword": "When ",
  "line": 26
});
formatter.step({
  "name": "User enters UserName and Password",
  "keyword": "And ",
  "line": 27
});
formatter.step({
  "name": "Message displayed Login Successfully",
  "keyword": "Then ",
  "line": 28
});
formatter.match({
  "location": "Test1_steps.user_is_on_Home_Page()"
});
formatter.result({
  "duration": 9019756129,
  "status": "passed"
});
formatter.match({
  "location": "Test1_steps.user_Navigate_to_LogIn_Page()"
});
formatter.result({
  "duration": 162824053,
  "status": "passed"
});
formatter.match({
  "location": "Test1_steps.user_enters_UserName_and_Password()"
});
formatter.result({
  "duration": 10175777582,
  "status": "passed"
});
formatter.match({
  "location": "Test1_steps.message_displayed_Login_Successfully()"
});
formatter.result({
  "duration": 1166435768,
  "status": "passed"
});
formatter.uri("Feature1/002Loadjob1.feature");
formatter.feature({
  "id": "load-job",
  "tags": [
    {
      "name": "@tag",
      "line": 20
    }
  ],
  "description": "",
  "name": "Load job",
  "keyword": "Feature",
  "line": 21,
  "comments": [
    {
      "value": "#Author: your.email@your.domain.com",
      "line": 1
    },
    {
      "value": "#Keywords Summary :",
      "line": 2
    },
    {
      "value": "#Feature: List of scenarios.",
      "line": 3
    },
    {
      "value": "#Scenario: Business rule through list of steps with arguments.",
      "line": 4
    },
    {
      "value": "#Given: Some precondition step",
      "line": 5
    },
    {
      "value": "#When: Some key actions",
      "line": 6
    },
    {
      "value": "#Then: To observe outcomes or validation",
      "line": 7
    },
    {
      "value": "#And,But: To enumerate more Given,When,Then steps",
      "line": 8
    },
    {
      "value": "#Scenario Outline: List of steps for data-driven as an Examples and \u003cplaceholder\u003e",
      "line": 9
    },
    {
      "value": "#Examples: Container for s table",
      "line": 10
    },
    {
      "value": "#Background: List of steps run before each of the scenarios",
      "line": 11
    },
    {
      "value": "#\"\"\" (Doc Strings)",
      "line": 12
    },
    {
      "value": "#| (Data Tables)",
      "line": 13
    },
    {
      "value": "#@ (Tags/Labels):To group Scenarios",
      "line": 14
    },
    {
      "value": "#\u003c\u003e (placeholder)",
      "line": 15
    },
    {
      "value": "#\"\"",
      "line": 16
    },
    {
      "value": "## (Comments)",
      "line": 17
    },
    {
      "value": "#Sample Feature Definition Template",
      "line": 19
    }
  ]
});
formatter.scenario({
  "id": "load-job;sucessfuly-load-the-job",
  "tags": [
    {
      "name": "@tag1",
      "line": 23
    }
  ],
  "description": "",
  "name": "Sucessfuly load the job",
  "keyword": "Scenario",
  "line": 24,
  "type": "scenario"
});
formatter.step({
  "name": "User is on home page",
  "keyword": "Given ",
  "line": 25
});
formatter.step({
  "name": "User Navigates to run queue page",
  "keyword": "When ",
  "line": 26
});
formatter.step({
  "name": "User seraches for a job",
  "keyword": "And ",
  "line": 27
});
formatter.step({
  "name": "User loads the job",
  "keyword": "And ",
  "line": 28
});
formatter.step({
  "name": "Message displayed job loaded successfuly",
  "keyword": "Then ",
  "line": 29
});
formatter.match({
  "location": "LoadJob_steps.user_is_on_Home_Page()"
});
formatter.result({
  "duration": 7442540936,
  "status": "passed"
});
formatter.match({
  "location": "LoadJob_steps.user_navigates_to_run_queue_page()"
});
formatter.result({
  "duration": 10091528040,
  "status": "passed"
});
formatter.match({
  "location": "LoadJob_steps.user_searches_for_a_job()"
});
formatter.result({
  "duration": 5267466078,
  "status": "passed"
});
formatter.match({
  "location": "LoadJob_steps.user_loads_the_job()"
});
formatter.result({
  "duration": 171628340,
  "status": "passed"
});
formatter.match({
  "location": "LoadJob_steps.message_displayed_job_loaded_successfuly()"
});
formatter.result({
  "duration": 1191686416,
  "status": "passed"
});
formatter.uri("Feature1/003Unloadjob1.feature");
formatter.feature({
  "id": "unload-job",
  "tags": [
    {
      "name": "@tag",
      "line": 20
    }
  ],
  "description": "",
  "name": "Unload job",
  "keyword": "Feature",
  "line": 21,
  "comments": [
    {
      "value": "#Author: your.email@your.domain.com",
      "line": 1
    },
    {
      "value": "#Keywords Summary :",
      "line": 2
    },
    {
      "value": "#Feature: List of scenarios.",
      "line": 3
    },
    {
      "value": "#Scenario: Business rule through list of steps with arguments.",
      "line": 4
    },
    {
      "value": "#Given: Some precondition step",
      "line": 5
    },
    {
      "value": "#When: Some key actions",
      "line": 6
    },
    {
      "value": "#Then: To observe outcomes or validation",
      "line": 7
    },
    {
      "value": "#And,But: To enumerate more Given,When,Then steps",
      "line": 8
    },
    {
      "value": "#Scenario Outline: List of steps for data-driven as an Examples and \u003cplaceholder\u003e",
      "line": 9
    },
    {
      "value": "#Examples: Container for s table",
      "line": 10
    },
    {
      "value": "#Background: List of steps run before each of the scenarios",
      "line": 11
    },
    {
      "value": "#\"\"\" (Doc Strings)",
      "line": 12
    },
    {
      "value": "#| (Data Tables)",
      "line": 13
    },
    {
      "value": "#@ (Tags/Labels):To group Scenarios",
      "line": 14
    },
    {
      "value": "#\u003c\u003e (placeholder)",
      "line": 15
    },
    {
      "value": "#\"\"",
      "line": 16
    },
    {
      "value": "## (Comments)",
      "line": 17
    },
    {
      "value": "#Sample Feature Definition Template",
      "line": 19
    }
  ]
});
formatter.scenario({
  "id": "unload-job;title-of-your-scenario",
  "tags": [
    {
      "name": "@tag1",
      "line": 24
    }
  ],
  "description": "",
  "name": "Title of your scenario",
  "keyword": "Scenario",
  "line": 25,
  "type": "scenario"
});
formatter.step({
  "name": "Job is loaded",
  "keyword": "Given ",
  "line": 26
});
formatter.step({
  "name": "Job is not running",
  "keyword": "And ",
  "line": 27
});
formatter.step({
  "name": "Job is suspended",
  "keyword": "When ",
  "line": 28
});
formatter.step({
  "name": "Message displayed job is suspended successfuly",
  "keyword": "Then ",
  "line": 29
});
formatter.match({
  "location": "UnloadJob_steps.job_is_loaded()"
});
formatter.result({
  "duration": 12584233679,
  "status": "passed"
});
formatter.match({
  "location": "UnloadJob_steps.Job_is_not_running()"
});
formatter.result({
  "duration": 30326525,
  "status": "passed"
});
formatter.match({
  "location": "UnloadJob_steps.Job_is_suspended()"
});
formatter.result({
  "duration": 355181831,
  "status": "passed"
});
formatter.match({
  "location": "UnloadJob_steps.Message_displayed_job_is_suspended_successfuly()"
});
formatter.result({
  "duration": 1157998708,
  "status": "passed"
});
formatter.uri("Feature1/004Logout1.feature");
formatter.feature({
  "id": "logout",
  "tags": [
    {
      "name": "@tag",
      "line": 20
    }
  ],
  "description": "",
  "name": "Logout",
  "keyword": "Feature",
  "line": 21,
  "comments": [
    {
      "value": "#Author: your.email@your.domain.com",
      "line": 1
    },
    {
      "value": "#Keywords Summary :",
      "line": 2
    },
    {
      "value": "#Feature: List of scenarios.",
      "line": 3
    },
    {
      "value": "#Scenario: Business rule through list of steps with arguments.",
      "line": 4
    },
    {
      "value": "#Given: Some precondition step",
      "line": 5
    },
    {
      "value": "#When: Some key actions",
      "line": 6
    },
    {
      "value": "#Then: To observe outcomes or validation",
      "line": 7
    },
    {
      "value": "#And,But: To enumerate more Given,When,Then steps",
      "line": 8
    },
    {
      "value": "#Scenario Outline: List of steps for data-driven as an Examples and \u003cplaceholder\u003e",
      "line": 9
    },
    {
      "value": "#Examples: Container for s table",
      "line": 10
    },
    {
      "value": "#Background: List of steps run before each of the scenarios",
      "line": 11
    },
    {
      "value": "#\"\"\" (Doc Strings)",
      "line": 12
    },
    {
      "value": "#| (Data Tables)",
      "line": 13
    },
    {
      "value": "#@ (Tags/Labels):To group Scenarios",
      "line": 14
    },
    {
      "value": "#\u003c\u003e (placeholder)",
      "line": 15
    },
    {
      "value": "#\"\"",
      "line": 16
    },
    {
      "value": "## (Comments)",
      "line": 17
    },
    {
      "value": "#Sample Feature Definition Template",
      "line": 19
    }
  ]
});
formatter.scenario({
  "id": "logout;successful-logout",
  "tags": [
    {
      "name": "@tag1",
      "line": 23
    }
  ],
  "description": "",
  "name": "Successful LogOut",
  "keyword": "Scenario",
  "line": 24,
  "type": "scenario"
});
formatter.step({
  "name": "User is on Home Page1",
  "keyword": "Given ",
  "line": 25
});
formatter.step({
  "name": "User LogOut from the Application",
  "keyword": "When ",
  "line": 26
});
formatter.step({
  "name": "Message displayed Logout Successfully",
  "keyword": "Then ",
  "line": 27
});
formatter.match({
  "location": "Logout1_steps.user_is_on_Home_Page1()"
});
formatter.result({
  "duration": 7028398866,
  "status": "passed"
});
formatter.match({
  "location": "Logout1_steps.user_LogOut_from_the_Application()"
});
formatter.result({
  "duration": 10153235719,
  "status": "passed"
});
formatter.match({
  "location": "Logout1_steps.message_displayed_Logout_Successfully()"
});
formatter.result({
  "duration": 1152939607,
  "status": "passed"
});
});