package stepDefinition1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import Common.quote;

import com.gui_auto.utilities.Locators;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import excel.Constant;
import excel.ExcelUtils;

public class Test1_steps {
	
	private static WebDriver driver;
	
	
	
	@Given("^User is on Home Page$")
	public void user_is_on_Home_Page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		System.setProperty("webdriver.chrome.driver","E:/Selenium/Automation/InputData/chromedriver.exe");
		 driver =  new ChromeDriver();
 
        //Put a Implicit wait, this means that any search for elements on the page could take the time the implicit wait is set for before throwing exception
 
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //String machineno="4207";
        //Launch the Online Store Website
        
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"Sheet1");
		String machineno= ExcelUtils.getCellData(1,2);
		//int jobnum = Integer.parseInt(jobno);
		machineno = machineno.replaceAll("[\'\"]", "");
    	System.out.println(machineno);
 
        driver.get("http://epscountwin2016/autocount/"+machineno+"/ui/index.html#/home");
	    //throw new PendingException();
	}

	@When("^User Navigate to LogIn Page$")
	public void user_Navigate_to_LogIn_Page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		// Find the element that's ID attribute is 'account'(My Account) 
		 
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath(Locators.getProperty(Locators.User_pic))).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    //throw new PendingException();
	}

	@When("^User enters UserName and Password$")
	public void user_enters_UserName_and_Password() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		// Enter Username on the element found by above desc.
		 
        //driver.findElement(By.id("log")).sendKeys("kar1"); 
 
        // Find the element that's ID attribute is 'pwd' (Password)
 
        // Enter Password on the element found by the above desc.
 
        //driver.findElement(By.id("pwd")).sendKeys("testingcucumber1");
 
        // Now submit the form. WebDriver will find the form for us from the element 
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"Sheet1");
		String un= ExcelUtils.getCellData(1,0);
		String quotedun = quote.quote(un);
		String xp_un="//p[contains(text(),"+quotedun+")]";
		//System.out.println(un);
		//System.out.println(xp_un);
        driver.findElement(By.xpath(xp_un)).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath(Locators.getProperty(Locators.Login_button))).click();
        Thread.sleep(5000);
	    //throw new PendingException();
	}

	@Then("^Message displayed Login Successfully$")
	public void message_displayed_Login_Successfully() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		 // Print a Log In message to the screen
		 
        System.out.println("Login Successfully");
	    //throw new PendingException();
        driver.quit(); 
	}
	
	
}
