package stepDefinition1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.gui_auto.utilities.Locators;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import excel.Constant;
import excel.ExcelUtils;



public class LoadJob_steps {
	
	private static WebDriver driver;
	
	@Given("^User is on home page$")
	public void user_is_on_Home_Page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		System.setProperty("webdriver.chrome.driver","E:/Selenium/Automation/InputData/chromedriver.exe");
		 driver =  new ChromeDriver();
 
        //Put a Implicit wait, this means that any search for elements on the page could take the time the implicit wait is set for before throwing exception
 
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
 
        //Launch the Online Store Website
 
        driver.get("http://epscountwin2016/autocount/4207/ui/index.html#/home");
	    //throw new PendingException();
	}
	@When("^User Navigates to run queue page$")
	public void user_navigates_to_run_queue_page() throws Throwable{
		
		Thread.sleep(5000);
		driver.findElement(By.xpath(Locators.getProperty(Locators.Runqueue_icon))).click();
		Thread.sleep(5000);
		
	}
    @When("^User seraches for a job$")
    public void user_searches_for_a_job() throws Throwable{
    	
    	//Thread.sleep(5000);
    	ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"Sheet1");
		String jobno= ExcelUtils.getCellData(1,1);
		//int jobnum = Integer.parseInt(jobno);
		jobno = jobno.replaceAll("[\'\"]", "");
    	System.out.println(jobno);
    	
    	driver.findElement(By.xpath(Locators.getProperty(Locators.Runqueue_search))).sendKeys(jobno);
    	Thread.sleep(5000);
    	driver.findElement(By.xpath(Locators.getProperty(Locators.Runqueue_Searchicon))).click();
    	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    	
    	
    	
    }
    @When("^User loads the job$")
    public void user_loads_the_job() throws Throwable{
    	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    	driver.findElement(By.xpath(Locators.getProperty(Locators.Runqueue_jobdetail))).click();
    	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    	driver.findElement(By.xpath(Locators.getProperty(Locators.Select_job_Continue))).click();
    	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    	Thread.sleep(5000);
    	if(driver.findElement(By.xpath(Locators.getProperty(Locators.QQ_popup))).isDisplayed())
    	{
    		System.out.println("Quality Question Popup is displayed... Closing");
    		Thread.sleep(5000);
    		driver.findElement(By.xpath(Locators.getProperty(Locators.Close_QQ_popup))).click();
    	}
    	if(driver.findElement(By.xpath(Locators.getProperty(Locators.Traceability_msg))).isDisplayed())
    	{
    		System.out.println("High Tracibility Job.. Adding input material");
    		Thread.sleep(5000);
    		driver.findElement(By.xpath(Locators.getProperty(Locators.Material_icon))).click();
    		Thread.sleep(5000);
    		driver.findElement(By.xpath(Locators.getProperty(Locators.Input_button))).click();
    		Thread.sleep(5000);
    		driver.findElement(By.xpath(Locators.getProperty(Locators.Material_id))).sendKeys("106388");;
    		Thread.sleep(5000);
    		driver.findElement(By.xpath(Locators.getProperty(Locators.Material_id))).sendKeys(Keys.ENTER);
    		Thread.sleep(5000);
    		if(driver.findElement(By.xpath(Locators.getProperty(Locators.Chk_Material_popup))).isDisplayed())
    		{
    			driver.findElement(By.xpath(Locators.getProperty(Locators.Chk_Material_close))).click();
    			Thread.sleep(5000);
    		}
    		driver.findElement(By.xpath(Locators.getProperty(Locators.Material_Select))).click();
    	}
    }
    @Then("^Message displayed job loaded successfuly$")
    public void message_displayed_job_loaded_successfuly() throws Throwable{
    	
    	System.out.println("Job Loaded Successfully");
    	driver.quit(); 
    }
}

