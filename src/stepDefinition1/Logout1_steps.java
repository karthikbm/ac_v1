package stepDefinition1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.gui_auto.utilities.Locators;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Logout1_steps {
	private static WebDriver driver;
	
	@Given("^User is on Home Page1$")
	public void user_is_on_Home_Page1() throws Throwable 
	{
	    // Write code here that turns the phrase above into concrete actions
		//System.setProperty("webdriver.chrome.driver","E:/Selenium/Automation/InputData/chromedriver.exe");
		 driver =  new ChromeDriver();
 
        //Put a Implicit wait, this means that any search for elements on the page could take the time the implicit wait is set for before throwing exception
 
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
 
        //Launch the Online Store Website
 
        driver.get("http://epscountwin2016/autocount/4207/ui/index.html#/home");
	    //throw new PendingException();
	}
	@When("^User LogOut from the Application$")
	public void user_LogOut_from_the_Application() throws Throwable 
	{
		//System.out.println("Click on Logout Image");
		Thread.sleep(5000);
		driver.findElement(By.xpath(Locators.getProperty(Locators.User_pic))).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(Locators.getProperty(Locators.Logout_button))).click();
		
	}
 
	@Then("^Message displayed Logout Successfully$")
	public void message_displayed_Logout_Successfully() throws Throwable 
	{
        System.out.println("LogOut Successfully");
        driver.quit(); 
	}

}
