package stepDefinition1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gui_auto.utilities.Locators;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Runjob1_steps {
	
	private static WebDriver driver;
	private static WebDriver driver1;
	
	@Given("^Job is loaded for running$")
	public void job_is_loaded() throws Throwable{
		 System.setProperty("webdriver.chrome.driver","E:/Selenium/Automation/InputData/chromedriver.exe");
		 driver =  new ChromeDriver();
         driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 
         driver.get("http://epscountwin2016/autocount/4207/ui/index.html#/home");
         Thread.sleep(5000);
         if(driver.findElement(By.xpath(Locators.getProperty(Locators.Object_unload))).isEnabled())
         {
        	 
        	 System.out.println("Job is loaded");
        	 
        	 
         }
         else
         {
        	 System.out.println("job is not loaded");
        	 driver.quit(); 
         }
	}
	
	@And("^Job is not running for starting the run$")
	public void Job_is_not_running() throws Throwable{
		if(driver.findElement(By.xpath(Locators.getProperty(Locators.speed0))).isDisplayed())
        {
			System.out.println("job is not running");
			 
	    }
		else
		{
			
			System.out.println("Job is running");
		    driver.quit();
		}
		
	}
	@Then("^Run the job from Simulator$")
	public void Run_the_job_from_simulator() throws Throwable{
		System.setProperty("webdriver.chrome.driver","E:/Selenium/Automation/InputData/chromedriver.exe");
		driver1 =  new ChromeDriver();
        driver1.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 
        driver1.get("http://epscountwin2016/autocount/4207/ui/sim.html");
        Thread.sleep(5000);
        //driver1.findElement(By.xpath(Locators.getProperty(Locators.Enable_sim))).click();
        //Thread.sleep(5000);
        driver1.findElement(By.xpath("//label[@for='EnableSim']")).click();
	    Thread.sleep(5000);
	    driver1.findElement(By.xpath("//input[@id='speedbox']")).sendKeys("1000");
	    Thread.sleep(5000);
	    driver1.findElement(By.xpath("//input[@id='speedbox']")).sendKeys(Keys.TAB);
	    Thread.sleep(5000);
	    driver1.findElement(By.xpath("//div[@title='Start Running']")).click();
	    Thread.sleep(10000);
	    //driver.findElement(By.xpath("//li[@id='setEmergency']/a")).click();
        //driver1.findElement(By.xpath(Locators.getProperty(Locators.Set_speed))).sendKeys("2000");
        //Thread.sleep(5000);
        //WebElement we1=driver1.findElement(By.xpath(Locators.getProperty(Locators.Canvas_speed)));
        //we1.sendKeys(Keys.TAB);
        //int width = we1.getSize().getWidth();
        //System.out.println(width);
        //Actions act = new Actions(driver1);
        //act.moveToElement(we1).moveByOffset((width/2)-2, 0).click().perform();
        //act.moveToElement(we1).moveByOffset(0,100).click().perform();
        //Thread.sleep(5000);
        //driver1.findElement(By.xpath(Locators.getProperty(Locators.Canvas_speed))).click();
        //Thread.sleep(5000);
        
        //driver1.findElement(By.xpath(Locators.getProperty(Locators.Start_run))).click();
        //Thread.sleep(10000);
        //Dimension size=driver.findElement(By.xpath("//div[@id='currentSpeed']/div[contains(text(),'0')]")).getSize();
        //System.out.println(size);
        //while(driver.findElements(By.xpath(Locators.getProperty(Locators.speed0))).size() == 0);
        //{
        	//driver1.navigate().refresh();
        	//Thread.sleep(5000);
        	//driver1.findElement(By.xpath(Locators.getProperty(Locators.Enable_sim))).click();
        	//Thread.sleep(5000);
        	//WebElement we2=driver1.findElement(By.xpath(Locators.getProperty(Locators.Canvas_speed)));
        	//act.moveToElement(we2).moveByOffset(0,100).click().perform();
            //driver1.findElement(By.xpath(Locators.getProperty(Locators.Set_speed))).sendKeys("3000");
            //Thread.sleep(5000);
            //WebElement we2=driver1.findElement(By.xpath(Locators.getProperty(Locators.Set_speed)));
            //we2.sendKeys(Keys.TAB);
           // Thread.sleep(5000);
           // driver1.findElement(By.xpath(Locators.getProperty(Locators.Start_run))).click();
           // Thread.sleep(10000);
       // }
        	
        System.out.println("Job is running");
     }
	
	@When("^To go count is Zero$")
	public void To_go_count_is_zero() throws Throwable{
		
		try
		{
			
			WebDriverWait wait = new WebDriverWait(driver, 6000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='toGoCounts']/div[text()='0']")));
			if(driver.findElement(By.xpath("//div[@class='toGoCounts']/div[text()='0']")).isDisplayed())
			{
				//driver1.findElement(By.xpath(Locators.getProperty(Locators.Emergency_stop))).click();
				driver1.findElement(By.xpath("//li[@id='setEmergency']/a")).click();
			}
		}
		catch(Exception e)
		{
			System.out.println("To go is still not reached Zero");
		}
		
	}
	@Then("^Display message job is stopped$")
	public void Display_message_job_is_stopped() throws Throwable{
		
		System.out.println("Job is stopped");
	}

}
