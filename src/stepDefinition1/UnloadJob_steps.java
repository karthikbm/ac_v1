package stepDefinition1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.gui_auto.utilities.Locators;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class UnloadJob_steps {
	
	private static WebDriver driver;
	
	@Given("^Job is loaded$")
	public void job_is_loaded() throws Throwable{
		 System.setProperty("webdriver.chrome.driver","E:/Selenium/Automation/InputData/chromedriver.exe");
		 driver =  new ChromeDriver();
         driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 
         driver.get("http://epscountwin2016/autocount/4207/ui/index.html#/home");
         Thread.sleep(5000);
         if(driver.findElement(By.xpath(Locators.getProperty(Locators.Object_unload))).isEnabled())
         {
        	 
        	 System.out.println("Job is loaded.... Suspending it");
        	 
        	 
         }
         else
         {
        	 System.out.println("job is not loaded");
        	 driver.quit(); 
         }
	}
	
	@And("^Job is not running$")
	public void Job_is_not_running() throws Throwable{
		if(driver.findElement(By.xpath(Locators.getProperty(Locators.speed0))).isDisplayed())
        {
			System.out.println("job is not running.... Suspending now");
			 
	    }
		else
		{
			
			System.out.println("Job is running");
		    driver.quit();
		}
		
	}
	@When("^Job is suspended$")
	public void Job_is_suspended() throws Throwable{
		
     driver.findElement(By.xpath(Locators.getProperty(Locators.Object_unload))).click();
   	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 
   	 driver.findElement(By.xpath(Locators.getProperty(Locators.Suspend_button))).click();
   	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 
   	 driver.findElement(By.xpath(Locators.getProperty(Locators.Confirm_button))).click();
   	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 
   	    if(driver.findElement(By.xpath(Locators.getProperty(Locators.TAIMIP_popup))).isDisplayed())
   	      {
   	       try
   	       {
   	    	   driver.findElement(By.xpath(Locators.getProperty(Locators.EIMNRTI))).click();
    	       driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 
    	       driver.findElement(By.xpath(Locators.getProperty(Locators.TAIMIP_Confirm_button))).click();
    	       driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
   	       }
   	    	catch(Exception e)
   	    	{
   	            driver.quit();
   	    	}
   	      }
		
	}
	@Then("^Message displayed job is suspended successfuly$")
	public void Message_displayed_job_is_suspended_successfuly() throws Throwable{
		
	
        
       	 System.out.println("job is Suspended");
       	 driver.quit(); 
        
		
	}
	

}
