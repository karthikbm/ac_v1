package com.gui_auto.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Locators {

	public final static String BASEURL = "BASEURL";

	public final static String PROPERTY_FILENAME = "config/Locators.properties";
	private static Properties _dsf_automation_properties = new Properties();

	public final static String XLS_DATA = "XLS_DATA";
		
	
	//home page login logout
	public static String User_pic = "User_pic";
	public static String Select_user = "Select_user";
	public static String Login_button = "Login_button";
	public static String Logout_button= "Logout_button";
	
	//runqueue page
	public static String Runqueue_icon = "Runqueue_icon";
	public static String Runqueue_search = "Runqueue_search";
	public static String Runqueue_Searchicon = "Runqueue_Searchicon";
	public static String Runqueue_jobdetail = "Runqueue_jobdetail";
	
	//Select job pop-up
	public static String Select_job_Continue = "Select_job_Continue";
	
	//Unload job
	public static String Object_unload = "Object_unload";
	
	//current job pop-up
	public static String Suspend_button = "Suspend_button";
	
	//Confirm Action pop-up
	public static String Confirm_button = "Confirm_button";
	
	//There are input materials in progress pop-up
	public static String EIMNRTI = "EIMNRTI";
	public static String TAIMIP_Confirm_button = "TAIMIP_Confirm_button";
	public static String TAIMIP_popup = "TAIMIP_popup";
	
	//opcodes
	public static String Running = "Running";
	
	//Gross count=0
	public static String Gross0 = "Gross0";
	public static String speed0 = "speed0";
	
	//Simulator
	public static String Enable_sim = "Enable_sim";
	public static String Set_speed = "Set_speed";
	public static String Start_run = "Start_run";
	public static String Emergency_stop = "Emergency_stop";
	public static String Canvas_speed = "Canvas_speed";
	
	//Traceability 
	
	public static String Traceability_msg = "Traceability_msg";
	
	//Material
	public static String Material_icon="Material_icon";
	public static String Input_button="Input_button";
	public static String Material_id="Material_id";
	public static String Chk_Material_popup="Chk_Material_popup";
	public static String Chk_Material_close="Chk_Material_close";
	public static String Material_Select="Material_Select";
	
	//Quality Question
	public static String QQ_popup="QQ_popup";
	public static String Close_QQ_popup="Close_QQ_popup";
	                                               
	/**
	 * Loads the properties file
	 */
	    static {
			try {
				_dsf_automation_properties.load(new FileInputStream(PROPERTY_FILENAME));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}

			assert !_dsf_automation_properties.isEmpty();
		}

		/**
		 * returns the value of the property denoted by key
		 * 
		 * @param key
		 * @return
		 */
		public static String getProperty(final String key) {
			String property = _dsf_automation_properties.getProperty(key);
			return property != null ? property.trim() : property;
		}
	
	public void test()
	{
		
	}

}

